<?php

namespace SGalinski\SgYoutube\Event;

final class PreYoutubeApiCallEvent {
	private $id;
	private $maxResultsWithFilters;
	private $apiKey;
	public function __construct(
		$id,
		$maxResultsWithFilters,
		$apiKey
	) {
		$this->id = $id;
		$this->maxResultsWithFilters = $maxResultsWithFilters;
		$this->apiKey = $apiKey;
	}
}
