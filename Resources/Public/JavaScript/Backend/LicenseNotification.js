/*
 *
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

import Notification from '@typo3/backend/notification.js';

const LicenseCheck = {
	init () {
		fetch(TYPO3.settings.ajaxUrls['sg_youtube::checkLicense'])
			.then((response) => {
				if (response.ok) {
					return response.json();
				}
			})
			.then((data) => {
				switch (data.error) {
					case 1: {
						Notification.error(data.title, data.message, 0);
						break;
					}
					case 2: {
						Notification.warning(data.title, data.message, 0);
					}
				}
			});
	},
};

LicenseCheck.init();
