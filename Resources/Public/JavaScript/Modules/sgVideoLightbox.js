import * as BasicLightbox from 'basiclightbox';

export default class SgVideoLightbox {
	/**
	 * Initializes the LightboxManager with the necessary parameters.
	 */
	constructor() {
		const videoItems = document.querySelectorAll('.sg-video-item');
		const isMobile = window.matchMedia('(max-width: 679px)').matches;

		videoItems.forEach((item) => {
			if (
				(item.dataset.disableLightboxMobile === '1' && isMobile) ||
				(item.dataset.disableLightbox === '1' && !isMobile)
			) {
				item.classList.remove('sg-video-item');
				item.addEventListener('click', SgVideoLightbox.disableLightbox.bind(this));
			}
		});

		Array.prototype.forEach.call(document.querySelectorAll('.sg-video-item'), (element) => {
			element.addEventListener('click', SgVideoLightbox.openLightbox);
		});
	}

	/**
	 * Opens the lightbox
	 *
	 * @param {Event} event
	 */
	static openLightbox(event) {
		event.preventDefault();
		const isShorts = event.target.closest('a')?.dataset?.isShorts === '1';
		switch (event.target.closest('a')?.dataset?.videoType) {
			case 'youtube': {
				SgVideoLightbox.openYouTubeLightBox(event, isShorts);
				break;
			}
			case 'vimeo': {
				SgVideoLightbox.openVimeoLightBox(event);
				break;
			}
			default:
			// do nothing
		}
	}

	/**
	 * Disables the lightbox
	 *
	 * @param {Event} event
	 */
	static disableLightbox(event) {
		event.preventDefault();
		const isShorts = event.target.closest('a')?.dataset?.isShorts === '1';
		switch (event.target.closest('a')?.dataset?.videoType) {
			case 'youtube': {
				SgVideoLightbox.disableYouTubeLightbox(event, isShorts);
				break;
			}
			case 'vimeo': {
				SgVideoLightbox.disableVimeoLightbox(event);
				break;
			}
			default:
			// do nothing
		}
	}

	/**
	 * Opens the lightbox for vimeo videos
	 *
	 * @param event
	 */
	static openVimeoLightBox(event) {
		let url = event.target.closest('.sg-video-item').href;
		url = SgVideoLightbox.includeAdditionalUrlParameters(
			url,
			event.target.closest('a').dataset.additionalUrlParameters,
		);
		url += '&autoplay=1&dnt=1';

		const instance = BasicLightbox.create(
			`<iframe class="sg-video-iframe sg-video-vimeo-iframe mfp-iframe" frameborder="0" allowfullscreen allow="autoplay; fullscreen *; picture-in-picture" src="${url}"></iframe>`,
			{
				closable: true,
			},
		);

		instance.show();
	}

	/**
	 * Opens the lightbox with the youtube player (uses youtube-nocookie)
	 *
	 * @param {Event} event
	 * @param {boolean} isShorts
	 */
	static openYouTubeLightBox(event, isShorts) {
		let url = event.target.closest('.sg-video-item').href;
		const videoId = SgVideoLightbox.getYouTubeVideoIdFromUrl(url);
		url = `https://www.youtube-nocookie.com/embed/${videoId}`;
		url = SgVideoLightbox.includeAdditionalUrlParameters(
			url,
			event.target.closest('a').dataset.additionalUrlParameters,
		);

		const iframeClass = isShorts
			? 'sg-video-iframe sg-video-youtube-iframe sg-video-youtube-shorts-iframe'
			: 'sg-video-iframe sg-video-youtube-iframe';

		const instance = BasicLightbox.create(
			`
			<iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
			class="${iframeClass}" src="${url}"></iframe>
		`,
			{
				closable: true,
			},
		);

		instance.show();
	}

	/**
	 * Replace the image with an iframe
	 *
	 * @param {Event} event
	 * @param {boolean} isShorts
	 *
	 */
	static disableYouTubeLightbox(event, isShorts) {
		event.preventDefault();
		const item = event.currentTarget;
		item.classList.add('no-lightbox');

		const videoId = SgVideoLightbox.includeAdditionalUrlParameters(
			SgVideoLightbox.getYouTubeVideoIdFromUrl(item.href),
			item.dataset.additionalUrlParameters,
		);
		const videoImage = item.querySelector('.sg-video__image');
		const originalWidth = videoImage.offsetWidth;
		const originalHeight = videoImage.offsetHeight;

		// Calculate iframe dimensions
		let iframeWidth = originalWidth;
		let iframeHeight = originalHeight;
		if (isShorts) {
			// Maintain 9:16 aspect ratio for Shorts
			iframeWidth = originalWidth;
			iframeHeight = Math.round((iframeWidth / 9) * 16);
		}

		const iframe = document.createElement('iframe');
		iframe.width = iframeWidth;
		iframe.height = iframeHeight;
		iframe.style.border = 'none';
		iframe.allowFullscreen = true;

		if (isShorts) {
			iframe.classList.add('sg-video-youtube-shorts-iframe');
		}

		iframe.allow =
			'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture';
		iframe.src = `https://www.youtube-nocookie.com/embed/${videoId}`;

		if (videoImage.parentElement.nodeName.toLowerCase() === 'picture') {
			videoImage.parentElement.replaceWith(iframe);
		} else {
			videoImage.replaceWith(iframe);
		}
	}

	/**
	 * Disables the lightbox for vimeo videos
	 *
	 * @param event
	 */
	static disableVimeoLightbox(event) {
		event.preventDefault();
		const item = event.currentTarget.closest('.sg-video__item');
		const link = item.querySelector('a');
		item.classList.add('no-lightbox');

		// This needs to be done, because the height of inline elements is always 0, if on auto...
		// $vimeoItem.css('display', 'block');

		const iframeUrl = SgVideoLightbox.includeAdditionalUrlParameters(
			link.href,
			link.dataset.additionalUrlParameters,
		);
		const thumbnailElement = item.querySelector('.sg-video__image');
		const width = thumbnailElement?.clientWidth;
		const height = thumbnailElement?.clientHeight;
		const nodes = document
			.createRange()
			.createContextualFragment(
				`<div class="sg-video-item sg-card-shadow" ` +
					`style="height: ${height}px; width: ${width}px;">` +
					`<div class="embed-container" style="padding-bottom: calc(${height} / ${width}  * 100%);">` +
					`<iframe ` +
					`width="${width}" height="${height}" ` +
					`src="${iframeUrl}&dnt=1" frameborder="0" ` +
					`allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>` +
					`</iframe>` +
					`</div></div>`,
			);
		thumbnailElement?.replaceWith(nodes);
	}

	/**
	 * Extracts the Vimeo Video ID form the URL
	 *
	 * @param {string} url
	 * @return {string}
	 */
	static getVimeoVideoIdFromUrl(url) {
		const regExp = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?(\d+)/;
		const parseUrl = regExp.exec(url);
		return parseUrl[5];
	}

	/**
	 * Prepares the given url, or returns null.
	 *
	 * @param {string} url
	 * @return {string|null}
	 */
	static getYouTubeVideoIdFromUrl(url) {
		// Match standard YouTube URL or Shorts URL
		const matches = url.match(/watch\?v=([^&?]*)(?:&list=([^&?]*))?|shorts\/([^&/?]+)/);
		if (!matches) {
			return null;
		}

		// Determine the video ID and construct the appropriate URL
		const videoId = matches[1] || matches[3];
		const listParameter = matches[2] ? `?list=${matches[2]}` : '';
		const autoplayParameters = listParameter ? '&autoplay=1&rel=0' : '?autoplay=1&rel=0';

		return `${videoId}${listParameter}${autoplayParameters}`;
	}

	/**
	 * Includes the additional URL parameters in the video url
	 *
	 * @param {string|null} url
	 * @param {string} _additionalUrlParameters
	 * @returns
	 */
	static includeAdditionalUrlParameters(url, _additionalUrlParameters = '') {
		if (!url) {
			return '';
		}

		if (!_additionalUrlParameters) {
			return url;
		}

		let additionalUrlParameters = _additionalUrlParameters;
		const beginsWithQuestionMark = additionalUrlParameters.charAt(0) === '?';
		const beginsWithAmpersand = additionalUrlParameters.charAt(0) === '&';

		if (beginsWithQuestionMark || beginsWithAmpersand) {
			additionalUrlParameters = additionalUrlParameters.slice(1);
		}

		return url.includes('?')
			? `${url}&${additionalUrlParameters}`
			: `${url}?${additionalUrlParameters}`;
	}
}
