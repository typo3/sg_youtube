/**
 * This handles the sg_youtube and sg_vimeo read more button
 */
export default class SgVideo {
	constructor(element, settings) {
		this.settings = settings;
		this.dom = {
			list: element,
			listItems: element.querySelectorAll('.sg-video__item'),
		};

		this.active = 0;

		this.dom.listItems.forEach((item, index) => {
			this.setupReadMore(index);
		});

		if (!this.settings.disableMinHeight) {
			this.checkImageSizes();
		}
	}

	/**
	 * Initialize the default sg video objects
	 */
	static initDefault() {
		/* eslint no-new: "off" */
		document.querySelectorAll('.sg-video__list--default').forEach((item) => {
			new SgVideo(item, {
				textSelector: '.sg-video__bodytext',
				descriptionSelector: '.sg-video__description',
				type: 'default',
			});
		});

		document.querySelectorAll('.sg-video__list--rows').forEach((item) => {
			new SgVideo(item, {
				textSelector: '.sg-video__description',
				descriptionSelector: '.sg-video__description',
				disableMinHeight: true,
			});
		});

		document.querySelectorAll('.sg-video__list--playlist').forEach((item) => {
			new SgVideo(item, {
				textSelector: '.sg-video__description',
				descriptionSelector: '.sg-video__description',
				disableMinHeight: true,
			});
		});

		document.querySelectorAll('.sg-video__highlight').forEach((item) => {
			new SgVideo(item, {
				textSelector: '.sg-video__bodytext',
				descriptionSelector: '.sg-video__description',
				disableMinHeight: true,
			});
		});

		document.querySelectorAll('.sg-video--single').forEach((item) => {
			new SgVideo(item, {
				textSelector: '.sg-video__description',
				descriptionSelector: '.sg-video__description',
			});
		});
	}

	/**
	 * The largest image height will be applied to the other images
	 */
	checkImageSizes() {
		let highestValue = 0;
		const images = [];
		this.dom.listItems.forEach((item) => {
			const image = item.querySelector('img');
			if (image && image.height > highestValue) {
				highestValue = image.height;
			}
			images.push(image);
		});

		images.forEach((image) => {
			image.style.minHeight = `${highestValue}px`;
		});
	}

	/**
	 * Sets up the read more button
	 *
	 * @param index
	 */
	setupReadMore(index) {
		const item = this.dom.listItems[index];
		const button = item.querySelector('.sg-video__read-more');
		const text = item.querySelector(this.settings.textSelector);

		if (!text) {
			if (button) {
				button.classList.add('disabled');
			}
			return;
		}

		if (!button) {
			return;
		}

		if (SgVideo.isTextTruncated(text, this.settings)) {
			button.classList.add('disabled');
			return;
		}

		button.addEventListener('click', () => this.showText(index));
	}

	/**
	 * Expands the description text
	 *
	 * @param index
	 */
	showText(index) {
		const item = this.dom.listItems[index];

		if (item.classList.contains('expanded')) {
			this.active -= 1;
			this.hideText(index);
			return;
		}

		this.active += 1;
		const button = item.querySelector('.sg-video__read-more');
		const text = item.querySelector(this.settings.textSelector);
		const description = text.matches(this.settings.descriptionSelector)
			? text
			: text.querySelector(this.settings.descriptionSelector);

		item.classList.add('expanded');
		text.classList.add('expanded');
		description.classList.add('expanded');
		button.classList.add('expanded');
		button.querySelector('.sg-video__read-more-text').textContent =
			button.dataset.buttonCloseText;
	}

	/**
	 * Collapses the description text
	 *
	 * @param index
	 */
	hideText(index) {
		const item = this.dom.listItems[index];
		const button = item.querySelector('.sg-video__read-more');
		const text = item.querySelector(this.settings.textSelector);
		const description = text.matches(this.settings.descriptionSelector)
			? text
			: text.querySelector(this.settings.descriptionSelector);

		text.classList.remove('expanded');
		description.classList.remove('expanded');
		button.classList.remove('expanded');
		button.querySelector('.sg-video__read-more-text').textContent =
			button.dataset.buttonOpenText;
		setTimeout(() => {
			if (this.settings.type === 'default' && this.active === 0) {
				this.dom.listItems.forEach((_item) => {
					_item.style.height = '';
					_item.style.zIndex = '';
				});
			}
			item.classList.remove('expanded');
		}, 200);
	}

	/**
	 * Check if the element was truncated by line-clamp
	 *
	 * @param {HTMLElement} element
	 * @param {Object} settings
	 * @return {boolean}
	 */
	static isTextTruncated(element, settings) {
		const descriptionElement = element.matches(settings.descriptionSelector)
			? element
			: element.querySelector(settings.descriptionSelector);

		return (
			descriptionElement && descriptionElement.scrollHeight <= descriptionElement.clientHeight
		);
	}
}
