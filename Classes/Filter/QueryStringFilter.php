<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\Filter;

class QueryStringFilter implements FilterInterface {
	protected array $filterValues;
	protected array $filterConfig;

	/**
	 * Constructor to initialize the filter with the query string.
	 *
	 * @param array $filterValues
	 * @param array $filterConfig
	 */
	public function __construct(array $filterValues, array $filterConfig) {
		$this->filterValues = $filterValues;
		$this->filterConfig = $filterConfig;
	}

	/**
	 * Modify the YouTube API request parameters (if necessary).
	 *
	 * @param array $parameters
	 */
	public function modifyRequest(array &$parameters): void {
		if (isset($this->filterValues['search']) && !empty($this->filterValues['search'])) {
			$parameters['q'] = $this->filterValues['search'];
		}
	}

	/**
	 * Optionally filter the API response, if needed.
	 * In this case, we won't filter the results, as we're modifying the query.
	 *
	 * @param array $data
	 */
	public function modifyResponse(array &$data): void {
	}

	public function getFilterValues(): array {
		return $this->filterValues;
	}

	public function setFilterValues(array $filterValues): void {
		$this->filterValues = $filterValues;
	}
}
