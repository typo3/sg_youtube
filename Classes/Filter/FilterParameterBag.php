<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\Filter;

class FilterParameterBag {
	protected array $parameters;
	protected array $filterInstances;
	protected bool $disableCache = FALSE;

	public function __construct(array $parameters = [], array $filterInstances = [], bool $disableCache = FALSE) {
		$this->parameters = $parameters;
		$this->filterInstances = $filterInstances;
		$this->disableCache = $disableCache;
	}

	public function get(string $key, $default = NULL) {
		return $this->parameters[$key] ?? $default;
	}

	public function set(string $key, $value): void {
		$this->parameters[$key] = $value;
	}

	public function all(): array {
		return $this->parameters;
	}

	public function remove(string $key): void {
		unset($this->parameters[$key]);
	}

	public function merge(array $parameters): void {
		$this->parameters = array_merge($this->parameters, $parameters);
	}

	public function getFilterInstances(): array {
		return $this->filterInstances;
	}

	public function setFilterInstances(array $filterInstances): void {
		$this->filterInstances = $filterInstances;
	}

	public function getDisableCache(): bool {
		return $this->disableCache;
	}

	public function setDisableCache(bool $disableCache): void {
		$this->disableCache = $disableCache;
	}
}
