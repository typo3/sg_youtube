<?php

/***************************************************************
 *  Copyright notice
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\Controller;

use Exception;
use InvalidArgumentException;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgYoutube\Event\AfterFilterVideosEvent;
use SGalinski\SgYoutube\Event\AfterMapCustomThumbnailsEvent;
use SGalinski\SgYoutube\Event\AfterYoutubeCallEvent;
use SGalinski\SgYoutube\Event\BeforeYoutubeCallEvent;
use SGalinski\SgYoutube\Filter\FilterParameterBag;
use SGalinski\SgYoutube\Service\CachedImageService;
use SGalinski\SgYoutube\Service\YoutubeService;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Service\ImageService;

/**
 *  The YouTube Controller
 */
class YoutubeController extends ActionController {
	/**
	 * @var YoutubeService
	 */
	protected $youtubeService;

	/**
	 * Injects the YouTube service
	 *
	 * @param YoutubeService $youtubeService
	 * @return void
	 */
	public function injectYoutubeService(YoutubeService $youtubeService) {
		$this->youtubeService = $youtubeService;
	}

	/**
	 * Renders the YouTube video view
	 *
	 * @return ResponseInterface
	 */
	public function indexAction(): ResponseInterface {
		$id = $this->settings['id'] ?? '';
		$filterIds = $this->settings['filterId'] ?? '';
		$maxResults = $this->settings['maxResults'] ?? 10;
		$apiKey = $this->settings['apiKey'] ?? '';
		$thumbnailType = $this->settings['thumbnailType'] ?? 'byAspectRatio';
		$aspectRatio = $this->settings['aspectRatio'] ?? '16:9';
		$showApiResult = (bool) ($this->settings['showApiResult'] ?? FALSE);
		$queryString = $this->settings['queryString'] ?? '';
		$isShorts = (bool) ($this->settings['isShorts'] ?? FALSE);
		$debugOutput = '';

		$youtubeParameters = [
			'id' => $id,
			'apiKey' => $apiKey,
			'queryString' => $queryString,
		];

		$filterIds = explode(',', $filterIds);
		foreach ($filterIds as &$filterId) {
			$filterId = trim($filterId, ' ');
		}

		unset($filterId);

		// Get input values
		$elementId = $this->request->getAttribute('currentContentObject')->data['uid'];
		$inputValues = $this->request->getQueryParams();
		$filterValues = [];
		foreach ($inputValues as $key => $value) {
			if (str_starts_with($key, $elementId . '__')) {
				[$elementUid, $filterName, $inputName] = explode('__', $key);
				if ((int) $elementUid === $elementId) {
					$filterValues[$filterName][$inputName] = $value;
				}
			}
		}

		$typo3Version = VersionNumberUtility::getCurrentTypo3Version();
		if (version_compare($typo3Version, '13.0.0', '<')) {
			$disableYoutubeCache = (bool) GeneralUtility::_GP('disableYoutubeCache');
		} else {
			$disableYoutubeCache = (bool) ($this->request->getParsedBody()['disableYoutubeCache'] ?? $this->request->getQueryParams()['disableYoutubeCache'] ?? NULL);
		}

		// Add third-party filters
		$filterInstances = $this->handleFrontendFilters($filterValues);

		$maxResultsWithFilters = (string) ((int) $maxResults + count($filterIds));
		$youtubeParameters['maxResults'] = $maxResultsWithFilters;

		// add Filter Values
		$youtubeParameters['filterValues'] = $filterValues;
		try {
			// Dispatch the BeforeYoutubeCallEvent
			$beforeYoutubeCallEvent = new BeforeYoutubeCallEvent($youtubeParameters);
			$this->eventDispatcher->dispatch($beforeYoutubeCallEvent);

			// Use the possibly modified parameters
			$jsonArray = $this->youtubeService->getJsonAsArray(
				new FilterParameterBag($youtubeParameters, $filterInstances, $disableYoutubeCache)
			);

			// Dispatch the AfterYoutubeCallEvent
			$afterYoutubeCallEvent = new AfterYoutubeCallEvent($jsonArray);
			$this->eventDispatcher->dispatch($afterYoutubeCallEvent);

			// Use the possibly modified jsonArray
			$jsonArray = $afterYoutubeCallEvent->getJsonArray();

			// Apply the 3rd party frontend filters to the response
			foreach ($filterInstances as $filterInstance) {
				$filterInstance->modifyResponse($jsonArray);
			}

			// Apply the filterIds setting from the plugin flexform
			$jsonArray = $this->applyFilterIds($jsonArray, $filterIds, $maxResults);

			// Dispatch the AfterFilterVideosEvent
			$afterFilterVideosEvent = new AfterFilterVideosEvent($jsonArray);
			$this->eventDispatcher->dispatch($afterFilterVideosEvent);

			// Use the possibly modified jsonArray
			$jsonArray = $afterFilterVideosEvent->getJsonArray();

			if ($showApiResult) {
				$debugOutput = nl2br($id . "\n\n" . print_r($jsonArray, TRUE));
			}

			$jsonArray['items'] = $this->youtubeService->mapArray(
				$jsonArray['items'],
				$id,
				$aspectRatio,
				$thumbnailType,
				$apiKey,
				$isShorts
			);

			$jsonArray['items'] = $this->mapJsonArrayWithPossibleCustomThumbnails($jsonArray['items']);

			// Dispatch the AfterGetJsonAsArrayEvent
			$afterMapCustomThumbnailsEvent = new AfterMapCustomThumbnailsEvent($jsonArray);
			$this->eventDispatcher->dispatch($afterMapCustomThumbnailsEvent);

			// Use the possibly modified jsonArray
			$jsonArray = $afterMapCustomThumbnailsEvent->getJsonArray();
		} catch (InvalidArgumentException $exception) {
			if ($exception->getCode() === 403 && $exception->getMessage() === 'No items found.') {
				return $this->returnNoVideosFoundResponse($debugOutput);
			}

			return $this->displayException($exception);
		} catch (Exception $exception) {
			return $this->displayException($exception);
		}

		if (count($jsonArray['items']) < 1) {
			return $this->returnNoVideosFoundResponse($debugOutput);
		}

		$cachedImageService = GeneralUtility::makeInstance(CachedImageService::class, 'youtube');
		foreach ($jsonArray['items'] as $key => $item) {
			if (isset($item['thumbnail'])) {
				$jsonArray['items'][$key]['thumbnail'] = $cachedImageService->getImage($item['thumbnail']);
			}
		}

		$this->view->assignMultiple(
			[
				'feed' => $jsonArray['items'],
				'response' => $jsonArray,
				'debugOutput' => $debugOutput,
				'showTitle' => (int) ($this->settings['showTitle'] ?? 1),
				'showDescription' => (int) ($this->settings['showDescription'] ?? 1),
				'pluginContentData' => $this->request->getAttribute('currentContentObject')->data
			]
		);

		return $this->htmlResponse();
	}

	/**
	 * Maps the given jsonArray thumbnails with the possible custom ones from the plugin settings.
	 *
	 * @param array $jsonArray
	 * @return array
	 */
	protected function mapJsonArrayWithPossibleCustomThumbnails(array $jsonArray): array {
		$contentElementUid = (int) $this->request->getAttribute('currentContentObject')->data['uid'];
		if ($contentElementUid <= 0) {
			return $jsonArray;
		}

		$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
		$fileObjects = $fileRepository->findByRelation(
			'tt_content',
			'settings.thumbnailImages',
			$contentElementUid
		);

		if (count($fileObjects) <= 0) {
			return $jsonArray;
		}

		/** @var FileReference $fileObject */
		foreach ($fileObjects as $index => $fileObject) {
			if (!isset($jsonArray[$index])) {
				break;
			}

			$cropString = '';
			if ($fileObject->hasProperty('crop') && $fileObject->getProperty('crop')) {
				$cropString = $fileObject->getProperty('crop');
			}

			$cropVariantCollection = CropVariantCollection::create((string) $cropString);
			$cropArea = $cropVariantCollection->getCropArea();
			$processingInstructions = [
				'crop' => $cropArea->isEmpty() ? NULL : $cropArea->makeAbsoluteBasedOnFile($fileObject),
			];
			$imageService = GeneralUtility::makeInstance(ImageService::class);
			$processedImage = $imageService->applyProcessingInstructions($fileObject, $processingInstructions);
			$jsonArray[$index]['thumbnail'] = $imageService->getImageUri($processedImage);
			$jsonArray[$index]['thumbnailImageObject'] = $fileObject;
		}

		return $jsonArray;
	}

	/**
	 * Applies the filterIds setting from the plugin configuration
	 *
	 * @param array $jsonArray
	 * @param array $filterIds
	 * @param mixed $maxResults
	 * @return array
	 */
	private function applyFilterIds(array $jsonArray, array $filterIds, mixed $maxResults): array {
		$jsonArray['items'] = array_filter($jsonArray['items'], function ($item) use ($filterIds) {
			$videoId = $item['snippet']['resourceId']['videoId'] ?? $item['id'];
			if (is_array($videoId)) {
				$videoId = $videoId['videoId'] ?? 0;
			}
			return !in_array($videoId, $filterIds, TRUE);
		});

		// Fix the array indexes from previous filtering
		$jsonArray['items'] = array_values($jsonArray['items']);
		while (count($jsonArray['items']) > $maxResults) {
			array_pop($jsonArray['items']);
		}
		return $jsonArray;
	}

	/**
	 * Applies the third-party filters
	 *
	 * @param array $filterValues
	 * @return array
	 */
	private function handleFrontendFilters(array $filterValues): array {
		if (!isset($this->settings['selectedFilters']) || !$this->settings['selectedFilters']) {
			return [];
		}

		$filters = $this->settings['filters'] ?? [];
		$selectedFilters = GeneralUtility::trimExplode(',', $this->settings['selectedFilters']);
		$filterDataTop = [];
		$filterDataBottom = [];
		$filterInstances = [];

		// Categorize filters by position
		foreach ($filters as $filterName => $filterConfig) {
			if (!in_array($filterName, $selectedFilters)) {
				continue;
			}
			$partialName = $filterConfig['partial'] ?? '';
			$label = $filterConfig['label'] ?? ucfirst($filterName);
			$position = $filterConfig['position'] ?? 'top'; // Default to top

			$filterData = [
				'partial' => $partialName,
				'label' => $label,
				'name' => $filterName,
				'options' => $filterConfig['options'] ?? [],
			];

			if (isset($filterConfig['defaultValues'])) {
				$filterData['defaultValues'] = $filterConfig['defaultValues'];

				foreach ($filterData['defaultValues'] as $input => $defaultValue) {
					if (!isset($filterValues[$filterName][$input])) {
						$filterValues[$filterName][$input] = $defaultValue;
					}
				}
			}

			if ($position === 'bottom') {
				$filterDataBottom[] = $filterData;
			} else {
				$filterDataTop[] = $filterData;
			}

			if (class_exists($filterConfig['filterClass'])) {
				$specificFilterValues = $filterValues[$filterName] ?? [];
				$filterInstance = GeneralUtility::makeInstance(
					$filterConfig['filterClass'],
					$specificFilterValues,
					$filterConfig
				);

				// Register filter to modify the request before API call
				$filterInstance->setFilterValues($specificFilterValues);
				$filterInstances[] = $filterInstance;
			}
		}

		// Get submit button settings
		$submitButton = $this->settings['submitButton'] ?? [];

		// Assign filters to view for rendering
		$this->view->assign('filtersTop', $filterDataTop);
		$this->view->assign('filtersBottom', $filterDataBottom);
		$this->view->assign('filtersCount', count($filters));
		$this->view->assign('submitButton', $submitButton);
		$this->view->assign('filterValues', $filterValues);
		return $filterInstances;
	}

	/**
	 * @param string $debugOutput
	 * @return ResponseInterface
	 */
	public function returnNoVideosFoundResponse(string $debugOutput): ResponseInterface {
		$this->view->assignMultiple(
			[
				'notFound' => TRUE,
				'debugOutput' => $debugOutput,
				'pluginContentData' => $this->request->getAttribute('currentContentObject')->data
			]
		);
		return $this->htmlResponse();
	}

	/**
	 * Displays an error message and exception
	 *
	 * @param InvalidArgumentException|Exception $exception
	 * @return ResponseInterface
	 */
	public function displayException(InvalidArgumentException|Exception $exception): ResponseInterface {
		return $this->htmlResponse('<div style="color: red;">' . $exception->getMessage() . '</div>');
	}
}
