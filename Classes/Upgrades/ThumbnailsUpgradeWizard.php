<?php

/**
 *
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

namespace SGalinski\SgYoutube\Upgrades;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * ThumbnailsUpgradeWizard
 */
#[UpgradeWizard('sgYoutube_thumbnailsUpgradeWizard')]
final class ThumbnailsUpgradeWizard implements UpgradeWizardInterface {
	/**
	 * Return the speaking name of this wizard
	 */
	public function getTitle(): string {
		return 'Migrate the thumbnails field name in the database';
	}

	/**
	 * Return the description for this wizard
	 */
	public function getDescription(): string {
		return 'This must be done in TYPO3 12 due to a bug and changes to how files fields are handled. See https://forge.typo3.org/issues/103885';
	}

	public function executeUpdate(): bool {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)
			?->getConnectionForTable('sys_file_reference');

		$queryBuilder = $connection->createQueryBuilder();
		$queryBuilder
			->update('sys_file_reference')
			->set('fieldname', 'settings.thumbnailImages')
			->where(
				$queryBuilder->expr()->eq(
					'fieldname',
					$queryBuilder->createNamedParameter('tx_sgyoutube_thumbnail_image')
				)
			);

		$queryBuilder->executeStatement();
		return TRUE;
	}

	/**
	 * @throws Exception
	 */
	public function updateNecessary(): bool {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)
			?->getConnectionForTable('sys_file_reference');

		$queryBuilder = $connection->createQueryBuilder();
		$queryBuilder->getRestrictions()->removeAll();

		$count = $queryBuilder
			->count('uid')
			->from('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq(
					'fieldname',
					$queryBuilder->createNamedParameter('tx_sgyoutube_thumbnail_image')
				)
			)
			->executeQuery()
			->fetchOne();

		return (int) $count > 0;
	}

	/**
	 * @return array|string[]
	 */
	public function getPrerequisites(): array {
		// Add your logic here
		return [];
	}
}
