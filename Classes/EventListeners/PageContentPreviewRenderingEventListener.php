<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\EventListeners;

use SGalinski\SgYoutube\Preview\PreviewService;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;

class PageContentPreviewRenderingEventListener {
	protected PreviewService $previewService;

	public function __construct(PreviewService $previewService) {
		$this->previewService = $previewService;
	}

	public function __invoke(PageContentPreviewRenderingEvent $event): void {
		if ($event->getTable() === 'tt_content') {
			$record = $event->getRecord();
			switch ($record['list_type']) {
				case 'sgyoutube_youtube':
					$event->setPreviewContent($this->previewService->getPluginView($record)->render('Backend'));
					break;
			}
		}
	}
}
