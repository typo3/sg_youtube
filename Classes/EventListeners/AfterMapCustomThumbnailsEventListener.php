<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\EventListeners;

use SGalinski\SgYoutube\Event\AfterMapCustomThumbnailsEvent;

class AfterMapCustomThumbnailsEventListener {
	public function __invoke(AfterMapCustomThumbnailsEvent $event): void {
		$jsonArray = $event->getJsonArray();
		// Add a custom thumbnail URL
		foreach ($jsonArray['items'] as $key => $item) {
			$jsonArray['items'][$key]['thumbnail'] = 'https://i.ytimg.com/vi/i4i2H9IJSAw/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDvqpGWePTP5GpQyXUtVNLKivagfg';
		}
		$event->setJsonArray($jsonArray);
	}
}
