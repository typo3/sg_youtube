<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\Backend;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use SGalinski\SgYoutube\Service\LicenceCheckService;
use TYPO3\CMS\Core\Http\ResponseFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class Ajax
 *
 * @package SGalinski\SgYoutube\Backend
 */
class Ajax {
	/**
	 * Checks whether the license is valid
	 *
	 * @param ServerRequestInterface $request
	 * @return ResponseInterface
	 */
	public function checkLicense(
		ServerRequestInterface $request
	): ResponseInterface {
		LicenceCheckService::setLastAjaxNotificationCheckTimestamp();
		$responseData = LicenceCheckService::getLicenseCheckResponseData(TRUE);
		$responseFactory = GeneralUtility::makeInstance(ResponseFactory::class);
		$response = $responseFactory->createResponse()->withHeader('Content-Type', 'application/json; charset=utf-8');
		$response->getBody()->write(json_encode($responseData));
		return $response;
	}
}
