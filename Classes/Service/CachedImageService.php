<?php

/**
 *
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

namespace SGalinski\SgYoutube\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * Handles the caching of images that are hosted externally
 *
 * NOTE: PURE COPY FROM PROJECT_BASE
 */
class CachedImageService {
	public const CACHED_IMAGES_DIRECTORY = 'fileadmin/Cache/';

	/**
	 * @var string
	 */
	protected $cacheDirectory = '';

	/**
	 * Creates a new instance that caches images in the $cacheDirectory sub-folder
	 *
	 * CachedImageService constructor.
	 *
	 * @param string $cacheDirectory
	 */
	public function __construct(string $cacheDirectory = '') {
		$this->cacheDirectory = ltrim($cacheDirectory, '/');
	}

	/**
	 * Returns the currently set subdirectory of the cache folder this service should write to
	 *
	 * @return string
	 */
	public function getCacheDirectory(): string {
		return $this->cacheDirectory;
	}

	/**
	 * Sets the subdirectory of the cache folder this service should write to
	 *
	 * @param string $cacheDirectory
	 * @return CachedImageService
	 */
	public function setCacheDirectory($cacheDirectory): CachedImageService {
		$this->cacheDirectory = $cacheDirectory;
		return $this;
	}

	/**
	 * Takes a URL to an image and returns the locally cached counterpart
	 * If the image is not yet in the cache, it will be downloaded.
	 *
	 * @param string $url
	 * @return string
	 */
	public function getImage($url): string {
		$cachedImageFolderPath = GeneralUtility::getFileAbsFileName(
			self::CACHED_IMAGES_DIRECTORY . $this->cacheDirectory . '/'
		);

		// do not cache local files
		$urlComponents = parse_url($url);
		if (!isset($urlComponents['scheme'])) {
			return $url;
		}

		// Create a hash based on the URL to identify the image
		$imageHash = \md5($url);
		// check if any kind of image with this hash is already in the cache-directory
		$cachedImage = \glob($cachedImageFolderPath . $imageHash . '.*');
		$cachedFileWebPath = '';
		if (count($cachedImage) <= 0) {
			// if the cache-directory has not been created yet, do so
			GeneralUtility::mkdir_deep($cachedImageFolderPath);

			if (@\copy($url, $cachedImageFolderPath . $imageHash)) {
				// copy without file extension for now
				$imageType = \exif_imagetype($cachedImageFolderPath . $imageHash);

				// figure out the file extension based on the image's mime-type
				$cachedFileName = $imageHash . \image_type_to_extension($imageType);
				$cachedFilePath = $cachedImageFolderPath . $cachedFileName;

				// correct name
				\rename($cachedImageFolderPath . $imageHash, $cachedFilePath);
				GeneralUtility::fixPermissions($cachedFilePath);
				$cachedFileWebPath = PathUtility::getAbsoluteWebPath($cachedFilePath);
			}
		} else {
			$cachedFileWebPath = PathUtility::getAbsoluteWebPath($cachedImage[0]);
		}

		return $cachedFileWebPath;
	}
}
