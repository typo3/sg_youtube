<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\Utility;

use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class FlexFormUtility {
	/**
	 * Get the dynamic options for the select field
	 *
	 * @param array $config
	 * @return array
	 */
	public function getOptions(array &$config) {
		$configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
		$fullTypoScript = $configurationManager->getConfiguration(
			ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
		);

		// Get the TypoScript configuration for the site
		$typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
		$typoScriptArray = $fullTypoScript;

		$pluginSettings = $typoScriptService->convertTypoScriptArrayToPlainArray(
			$typoScriptArray['plugin.']['tx_sgyoutube.']['settings.']
		);

		// Fetch the TypoScript settings (e.g., settings.selectOptions)
		$selectOptions = array_keys($pluginSettings['filters']);

		// Populate the select options dynamically
		foreach ($selectOptions as $option) {
			$config['items'][] = [$option, $option];
		}

		return $config;
	}
}
