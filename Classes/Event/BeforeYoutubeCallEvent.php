<?php

/**
 *
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

namespace SGalinski\SgYoutube\Event;

/**
 * Called before the call to the YouTube API
 */
final class BeforeYoutubeCallEvent {
	private array $parameters;

	private int $maxResultsWithFilters;

	private string $queryString;

	private string $apiKey;

	private mixed $id;

	public function __construct(array $parameters) {
		$this->parameters = $parameters;
	}

	public function getParamters(): array {
		return $this->parameters;
	}

	/**
	 * @return mixed
	 */
	public function getId(): mixed {
		return $this->id;
	}

	/**
	 * @param $id
	 * @return void
	 */
	public function setId($id): void {
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getMaxResultsWithFilters(): int {
		return $this->maxResultsWithFilters;
	}

	/**
	 * @param int $maxResultsWithFilters
	 * @return void
	 */
	public function setMaxResultsWithFilters(int $maxResultsWithFilters): void {
		$this->maxResultsWithFilters = $maxResultsWithFilters;
	}

	/**
	 * @return string
	 */
	public function getApiKey(): string {
		return $this->apiKey;
	}

	/**
	 * @param string $apiKey
	 * @return void
	 */
	public function setApiKey(string $apiKey): void {
		$this->apiKey = $apiKey;
	}

	/**
	 * @return string
	 */
	public function getQueryString(): string {
		return $this->queryString;
	}

	/**
	 * @param string $queryString
	 * @return void
	 */
	public function setQueryString(string $queryString): void {
		$this->queryString = $queryString;
	}
}
