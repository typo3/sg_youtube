<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the AY project. The AY project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * View helper that takes a URL and adds the parameters respecting the separator
 *
 * Example:
 * {namespace yt=SGalinski\SgYoutube\ViewHelpers}
 * <yt:urlWithQueryParameters url="https://www.youtube.com/?v=xyz" parameters="?enablejsapi=1&origin=https://demo.sgalinski.de" />
 * Result: https://www.youtube.com/?v=xyz&enablejsapi=1&origin=https://demo.sgalinski.de
 */
class UrlWithQueryParametersViewHelper extends AbstractViewHelper {
	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('url', 'string', 'The url to add the query parameters to', TRUE);
		$this->registerArgument('parameters', 'string', 'The query parameters to add', FALSE, '');
	}

	/**
	 * Returns the url with the added query parameters
	 *
	 * @return string
	 */
	public function render(): string {
		$url = $this->arguments['url'];
		$additionalUrlParameters = $this->arguments['parameters'];

		if ($additionalUrlParameters === '') {
			return $url;
		}

		$beginsWithQuestionMark = $additionalUrlParameters[0] === '?';
		$beginsWithAmpersand = $additionalUrlParameters[0] === '&';

		if ($beginsWithQuestionMark || $beginsWithAmpersand) {
			$additionalUrlParameters = substr($additionalUrlParameters, 1);
		}

		return str_contains($url, '?')
			? $url . '&' . $additionalUrlParameters
			: $url . '?' . $additionalUrlParameters;
	}
}
