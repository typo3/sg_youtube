<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Use this ViewHelper to get the opposite of the f:format.crop result
 */
final class CropOppositeViewHelper extends AbstractViewHelper {
	use CompileWithRenderStatic;

	/**
	 * The output may contain HTML and can not be escaped.
	 *
	 * @var bool
	 */
	protected $escapeOutput = FALSE;

	public function initializeArguments(): void {
		$this->registerArgument('maxCharacters', 'int', 'Place where to truncate the string', TRUE);
		$this->registerArgument('append', 'string', 'What to append, if truncation happened', FALSE, '&hellip;');
		$this->registerArgument(
			'respectWordBoundaries',
			'bool',
			'If TRUE and division is in the middle of a word, the remains of that word is removed.',
			FALSE,
			TRUE
		);
	}

	public static function renderStatic(
		array $arguments,
		\Closure $renderChildrenClosure,
		RenderingContextInterface $renderingContext
	): string {
		$maxCharacters = (int) $arguments['maxCharacters'];
		$append = (string) $arguments['append'];
		$respectWordBoundaries = (bool) ($arguments['respectWordBoundaries']);
		$stringToTruncate = (string) $renderChildrenClosure();

		return self::crop(
			content: $stringToTruncate,
			numberOfChars: $maxCharacters,
			replacementForEllipsis: $append,
			cropToSpace: $respectWordBoundaries
		);
	}

	/**
	 * Implements "cropHTML" which is a modified "substr" function allowing to limit a string length to a certain number
	 * of chars (from either start or end of string) and having a pre/postfix applied if the string really was cropped.
	 *
	 * Note:  Crop is done without properly respecting html tags and entities.
	 *
	 * @param string $content The string to perform the operation on
	 * @param int $numberOfChars Max number of chars of the string. Negative value means cropping from end of string.
	 * @param string $replacementForEllipsis The pre/postfix string to apply if cropping occurs.
	 * @param bool $cropToSpace If true then crop will be applied at nearest space.
	 * @return string The processed input value.
	 */
	private static function crop(
		string $content,
		int $numberOfChars,
		string $replacementForEllipsis,
		bool $cropToSpace
	): string {
		if (!$numberOfChars || $numberOfChars < 1 || !(mb_strlen($content, 'utf-8') > abs($numberOfChars))) {
			return $content;
		}

		// cropping from the left side of content, appending replacementForEllipsis
		$croppedContent = mb_substr($content, 0, $numberOfChars, 'utf-8');
		$truncatePosition = $cropToSpace ? mb_strrpos($croppedContent, ' ', 0, 'utf-8') : FALSE;
		$cropResult = $truncatePosition > 0
			? mb_substr($content, 0, $truncatePosition, 'utf-8')
			: $content;

		return str_replace($cropResult, '', $content);
	}
}
