<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\ViewHelpers;

use Exception;
use SimpleXMLElement;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class RenderSvgViewHelper
 */
class RenderSvgViewHelper extends AbstractTagBasedViewHelper {
	/**
	 * Name of the tag to be created by this view helper
	 *
	 * @var string
	 * @api
	 */
	protected $tagName = 'svg';
	/**
	 * @var string
	 */
	protected $xmlInfo = '<?xml version="1.0"?>';

	/**
	 * @var array
	 */
	protected static array $svgInstances = [];

	/**
	 * The directory path where the SVGs are located
	 *
	 * @var string
	 */
	protected $directoryPath = __DIR__ . '/../../Resources/Public/Icons/';

	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument(
			'name',
			'string',
			'The SVG name, also supports folders (e.g. fontawesome/solid/ad)',
			TRUE
		);
		$this->registerArgument(
			'color',
			'string',
			'The color',
			FALSE,
			'currentColor'
		);
		$this->registerArgument(
			'colorAttribute',
			'string',
			'The color target to affect: fill, stroke. Leave empty for both'
		);
		$this->registerArgument('id', 'string', 'The HTML id attribute');
		$this->registerArgument('path', 'string', 'Path to the SVG file if not in default');
		$this->registerArgument('class', 'string', 'The HTML class attribute');
		$this->registerArgument('title', 'string', 'The HTML title attribute');
		$this->registerArgument('width', 'string', 'The HTML width attribute');
		$this->registerArgument('height', 'string', 'The HTML height attribute');
		$this->registerArgument('viewBox', 'string', 'The HTML viewBox attribute');
		$this->registerArgument(
			'viewBoxOnly',
			'boolean',
			'If only the viewBox attribute should be used instead of width/height'
		);
		$this->registerArgument('stroke-width', 'string', 'The HTML stroke-width attribute');
		$this->registerArgument('style', 'string', 'Inline CSS styles');
		$this->registerArgument('use', 'string', 'Inline CSS styles');
		$this->registerArgument(
			'createNewInstance',
			'boolean',
			'Creates a new instance without reusing other ones'
		);
		$this->registerArgument('preserveColors', 'boolean', 'Preserves the original colors');
		$this->registerArgument(
			'createColorAttribute',
			'boolean',
			'Creates the color target for cases when it doesn\'t exist.',
			FALSE,
			FALSE
		);
		$this->registerArgument('aria-label', 'string', 'The HTML aria-label attribute');
		$this->registerArgument('aria-labelledby', 'string', 'The HTML aria-labelledby attribute');
		$this->registerArgument('role', 'string', 'The HTML role attribute');
		$this->registerArgument('aria-hidden', 'string', 'The HTML aria-hidden attribute');
		$this->registerArgument('focusable', 'string', 'The HTML focusable attribute');
	}

	/**
	 * Render the SVG file as an inline SVG element.
	 *
	 * @return string The rendered SVG element
	 * @throws Exception
	 */
	public function render(): string {
		$name = $this->arguments['name'];
		$path = $this->arguments['path'];
		$width = $this->arguments['width'];
		$height = $this->arguments['height'];
		$viewBox = $this->arguments['viewBox'];
		$viewBoxOnly = $this->arguments['viewBoxOnly'];
		$strokeWidth = $this->arguments['stroke-width'];
		$color = $this->arguments['color'];
		$colorAttribute = $this->arguments['colorAttribute'];
		$id = $this->arguments['id'];
		$class = $this->arguments['class'];
		$style = $this->arguments['style'];
		$title = $this->arguments['title'];
		$createNewInstance = $this->arguments['createNewInstance'];
		$preserveColors = $this->arguments['preserveColors'];
		$createColorAttribute = $this->arguments['createColorAttribute'];
		$ariaLabel = $this->arguments['aria-label'];
		$ariaLabelledby = $this->arguments['aria-labelledby'];
		$role = $this->arguments['role'];
		$ariaHidden = $this->arguments['aria-hidden'];
		$focusable = $this->arguments['focusable'];

		$src = $path ?? $this->directoryPath . '/' . $name . '.svg';

		// Get the content of the SVG file
		$content = file_get_contents($src);

		// Create a unique ID for the SVG element
		if (!$id) {
			$id = 'svg-' . md5($src . $width . $height . $color);
		}

		$clipPathId = 'clipPath-' . md5($src . $width . $height . $class . $id);

		// Load the SVG into a SimpleXMLElement object
		$svg = new SimpleXMLElement($content);

		$this->makeClipPathIdUnique($svg, $clipPathId);

		// Set the attributes of the SVG element
		if ($width > 0) {
			$this->addOrReplaceAttribute($svg, 'width', $width);
		}

		if ($height > 0) {
			$this->addOrReplaceAttribute($svg, 'height', $height);
		}

		if (!empty($viewBox)) {
			$this->addOrReplaceAttribute($svg, 'viewBox', $viewBox);
		}

		if ($viewBoxOnly) {
			unset($svg->attributes()->width, $svg->attributes()->height);
		}

		if ($strokeWidth > 0) {
			$this->setStrokeWidth($svg, $strokeWidth);
		}

		if (!empty($color) && !$preserveColors) {
			switch ($colorAttribute) {
				case 'fill':
					$this->setFill($svg, $color, $createColorAttribute);
					break;
				case 'stroke':
					$this->setStroke($svg, $color, $createColorAttribute);
					break;
				default:
					$this->setFill($svg, $color, $createColorAttribute);
					$this->setStroke($svg, $color, $createColorAttribute);
			}
		}

		if ($style) {
			$this->addOrReplaceAttribute($svg, 'style', $style);
		}

		if ($class) {
			$this->addOrReplaceAttribute($svg, 'class', $class);
		}

		if ($title) {
			$this->addOrReplaceAttribute($svg, 'title', $title);
		}

		if ($createNewInstance) {
			return str_replace($this->xmlInfo, '', $svg->asXML());
		}

		if ($ariaLabel) {
			$this->addOrReplaceAttribute($svg, 'aria-label', $ariaLabel);
		}

		if ($ariaLabelledby) {
			$this->addOrReplaceAttribute($svg, 'aria-labelledby', $ariaLabelledby);
		}

		if ($role) {
			$this->addOrReplaceAttribute($svg, 'role', $role);
		}

		if ($ariaHidden) {
			$this->addOrReplaceAttribute($svg, 'aria-hidden', $ariaHidden);
		}

		if ($focusable) {
			$this->addOrReplaceAttribute($svg, 'focusable', $focusable);
		}

		// Extract the SVG contents
		$contents = $this->getContents($svg, TRUE);

		// Check if the SVG has already been rendered and use the <use> tag if possible
		if (isset(static::$svgInstances[$id])) {
			$use = $svg->addChild('use');
			// The boolean conversion of SimpleXMLElement is broken, therefore we MUST use instanceof
			if ($use instanceof SimpleXMLElement) {
				$use->addAttribute('href', '#' . $id);
			}

			return str_replace($this->xmlInfo, '', $svg->asXML());
		}

		// Add the unique ID to the list of rendered SVGs
		static::$svgInstances[$id] = $id;

		$contentsElement = new SimpleXMLElement($contents);
		$group = $svg->addChild('g');
		// The boolean conversion of  SimpleXMLElement is broken, therefore we MUST explicitly check against null
		if ($group === NULL) {
			return str_replace($this->xmlInfo, '', $svg->asXML());
		}

		$group->addAttribute('id', $id);
		$this->xmlAdopt($group, $contentsElement);

		return str_replace($this->xmlInfo, '', $svg->asXML());
	}

	/**
	 * Set the fill color of an SVG element.
	 *
	 * @param SimpleXMLElement $element The SVG element
	 * @param string $fill The fill color to set
	 * @param boolean $createColorAttribute If the attribute should be added
	 */
	protected function setFill(SimpleXMLElement $element, string $fill, bool $createColorAttribute): void {
		foreach ($element->children() as $child) {
			$this->setFill($child, $fill, $createColorAttribute);
		}
		if ($createColorAttribute) {
			$element->addAttribute('fill', $fill);
		} elseif (isset($element->attributes()->fill)) {
			$element->attributes()->fill = $fill;
		}
	}

	/**
	 * Set the stroke color of an SVG element.
	 *
	 * @param SimpleXMLElement $element The SVG element
	 * @param string $stroke The stroke color to set
	 * @param boolean $createColorAttribute If the attribute should be added
	 */
	protected function setStroke(SimpleXMLElement $element, string $stroke, bool $createColorAttribute): void {
		foreach ($element->children() as $child) {
			$this->setStroke($child, $stroke, $createColorAttribute);
		}
		if ($createColorAttribute) {
			$element->addAttribute('stroke', $stroke);
		} elseif (isset($element->attributes()->stroke)) {
			$element->attributes()->stroke = $stroke;
		}
	}

	/**
	 * Set the stroke width of an SVG element.
	 *
	 * @param SimpleXMLElement $element The SVG element
	 * @param string $strokeWidth The stroke width to set
	 */
	protected function setStrokeWidth(SimpleXMLElement $element, string $strokeWidth): void {
		foreach ($element->children() as $child) {
			$this->setStrokeWidth($child, $strokeWidth);
		}

		foreach ($element->path as $path) {
			$this->addOrReplaceAttribute($path, 'stroke-width', $strokeWidth);
		}
	}

	/**
	 * Gets the contents of the SVG file
	 *
	 * @param SimpleXMLElement $svg
	 * @param bool $removeNode
	 * @return string
	 */
	private function getContents(SimpleXMLElement $svg, bool $removeNode = FALSE): string {
		$contents = '';
		foreach ($svg->children() as $child) {
			$contents .= $child->asXML() . "\n";
			if ($removeNode) {
				$dom = dom_import_simplexml($child);
				$dom->parentNode->removeChild($dom);
			}
		}

		return $contents;
	}

	/**
	 * Inserts a new SimpleXMLElement at the given root
	 *
	 * @param SimpleXMLElement $root
	 * @param SimpleXMLElement $newElement
	 * @return void
	 */
	private function xmlAdopt(SimpleXMLElement $root, SimpleXMLElement $newElement): void {
		$node = $root->addChild($newElement->getName(), (string) $newElement);
		if ($node === NULL) {
			return;
		}

		foreach ($newElement->attributes() as $attr => $value) {
			$node->addAttribute($attr, $value);
		}

		foreach ($newElement->children() as $ch) {
			$this->xmlAdopt($node, $ch);
		}
	}

	/**
	 * Adds or replaces an attribute
	 *
	 * @param SimpleXMLElement $element
	 * @param string $attributeName
	 * @param string $attributeValue
	 * @return void
	 */
	private function addOrReplaceAttribute(
		SimpleXMLElement $element,
		string $attributeName,
		string $attributeValue
	): void {
		if (isset($element[$attributeName])) {
			// Replace existing attribute value
			unset($element[$attributeName]);
		}

		$element->addAttribute($attributeName, $attributeValue);
	}

	/**
	 * Makes the clipPath ID unique
	 */
	private function makeClipPathIdUnique(SimpleXMLElement $xml, $newId): void {
		// Loop through the children to find the <g> element and update the clip-path attribute
		foreach ($xml->children() as $child) {
			if ($child->getName() === 'g') {
				$this->addOrReplaceAttribute($child, 'clip-path', "url(#$newId)");
			}
		}

		// Now loop through <defs> to find <clipPath> and update its id
		foreach ($xml->children() as $child) {
			if ($child->getName() === 'defs') {
				foreach ($child->children() as $defChild) {
					if ($defChild->getName() === 'clipPath') {
						$this->addOrReplaceAttribute($defChild, 'id', $newId);
					}
				}
			}
		}
	}
}
