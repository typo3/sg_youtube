<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the AY project. The AY project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgYoutube\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class StructuredVideoDataViewHelper
 */
class StructuredVideoDataViewHelper extends AbstractTagBasedViewHelper {
	/**
	 * Name of the tag to be created by this view helper
	 *
	 * @var string
	 * @api
	 */
	protected $tagName = 'script';

	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('videoArray', 'array', 'An array of videos', TRUE, []);
		$this->registerArgument('arrayType', 'string', 'Either "youtube" or "vimeo"', FALSE, 'youtube');
	}

	/**
	 * Takes the provided array of videos and creates an array for structured data JSON
	 *
	 * @return string
	 */
	public function render(): string {
		$this->escapeOutput = FALSE;
		$videoArray = $this->arguments['videoArray'] ?? [];
		$arrayType = $this->arguments['arrayType'] ?? 'youtube';

		$structuredData = [];
		if ($arrayType === 'youtube') {
			foreach ($videoArray as $video) {
				$structuredData[] = [
					'@type' => 'VideoObject',
					'name' => $video['title'],
					'description' => $video['description'],
					'thumbnailUrl' => $video['thumbnail'],
					'contentUrl' => $video['url'],
					'uploadDate' => $video['publishedAt'],
				];
			}
		} else {
			foreach ($videoArray as $video) {
				$structuredData[] = [
					'@type' => 'VideoObject',
					'name' => $video['name'],
					'description' => $video['description'],
					'thumbnailUrl' => $video['pictures']['sizes'][count($video['pictures']['sizes']) - 1]['link'],
					'contentUrl' => $video['link'],
					'uploadDate' => $video['release_time'],
				];
			}
		}

		$this->tag->addAttribute('type', 'application/ld+json');
		$this->tag->setContent(
			'{"@context": "http://schema.org", "@type": "WebPage", "video": ' . json_encode($structuredData) . '}'
		);
		return $this->tag->render();
	}
}
