<?php

use SGalinski\SgYoutube\Backend\Ajax;

return [
	'sg_youtube::checkLicense' => [
		'path' => '/sg_youtube/checkLicense',
		'target' => Ajax::class . '::checkLicense',
	],
];
