<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

$pluginSignature = ExtensionUtility::registerPlugin(
	'SgYoutube',
	'Youtube',
	'YouTube Videos',
	'extension-sg_youtube',
	'plugins',
	'LLL:EXT:sg_youtube/Resources/Private/Language/locallang.xlf:youtubePluginDescription'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:sg_youtube/Configuration/FlexForms/flexform_sgyoutube_youtube.xml'
);
