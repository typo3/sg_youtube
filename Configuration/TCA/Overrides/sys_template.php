<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
	'sg_youtube',
	'Configuration/TypoScript',
	'SG Youtube'
);
