<?php

use TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider;

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */

$iconList = [];
foreach (['extension-sg_youtube' => 'sg-youtube.png'] as $identifier => $path) {
	$iconList[$identifier] = [
		'provider' => BitmapIconProvider::class,
		'source' => 'EXT:sg_youtube/Resources/Public/Icons/' . $path,
	];
}

return $iconList;
