<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SGalinski\SgYoutube\Event\AfterFilterVideosEvent;
use SGalinski\SgYoutube\Event\AfterMapCustomThumbnailsEvent;
use SGalinski\SgYoutube\Event\AfterVimeoCallEvent;
use SGalinski\SgYoutube\Event\BeforeVimeoCallEvent;
use SGalinski\SgYoutube\EventListeners\AfterBackendPageRenderEventListener;
use SGalinski\SgYoutube\EventListeners\AfterFilterVideosEventListener;
use SGalinski\SgYoutube\EventListeners\AfterMapCustomThumbnailsEventListener;
use SGalinski\SgYoutube\EventListeners\AfterYoutubeCallEventListener;
use SGalinski\SgYoutube\EventListeners\BeforeYoutubeCallEventListener;
use SGalinski\SgYoutube\EventListeners\PageContentPreviewRenderingEventListener;
use SGalinski\SgYoutube\Form\Element\LicenceStatus;
use SGalinski\SgYoutube\Service\YoutubeService;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Backend\Controller\Event\AfterBackendPageRenderEvent;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgYoutube\\', __DIR__ . '/../Classes/');
	$services->set(YoutubeService::class)
		->args(['$cache' => service('cache.sgyoutube_cache')])->public()->autowire(FALSE);
	$services->set('cache.sgyoutube_cache')
		->class(FrontendInterface::class)
		->factory([service(CacheManager::class), 'getCache'])
		->args(['sgyoutube_cache']);
	$services->set(LicenceStatus::class)
		->autowire(FALSE)
		->autoconfigure(FALSE);
	$services->set(PageContentPreviewRenderingEventListener::class)
		->tag('event.listener', ['event' => PageContentPreviewRenderingEvent::class]);
	$services->set(AfterBackendPageRenderEventListener::class)
		->tag('event.listener', ['event' => AfterBackendPageRenderEvent::class]);

	// Uncomment the following code to try out the example EventListeners
	/*
	$services->set(BeforeYoutubeCallEventListener::class)
		->tag('event.listener', ['event' => BeforeYoutubeCallEvent::class]);
	$services->set(AfterYoutubeCallEventListener::class)
		->tag('event.listener', ['event' => AfterYoutubeCallEvent::class]);
	$services->set(AfterFilterVideosEventListener::class)
		->tag('event.listener', ['event' => AfterFilterVideosEvent::class]);
	$services->set(AfterMapCustomThumbnailsEventListener::class)
		->tag('event.listener', ['event' => AfterMapCustomThumbnailsEvent::class]);
	*/
};
