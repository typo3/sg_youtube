## Version 7

- Dropped TYPO3 10 and 11 support
- Dropped PHP 7 support

## Version 6

- Requires a license key from now on
- Removed dependencies to project_theme
- Removed dependencies to project_theme_lightbox
- TYPO3 12 support

## Version 5

- `youtubeLightbox.js` (deprecated since 4.4.0) using magnific-popup and jQuery removed in favor
  of `sgVideoLightbox.js` (vanilla JS).
- Extension `project_theme_lightbox` required starting with version 5.0.0.
- Dropped TYPO3 9 Support
- Dropped php 7.3 Support

## Version 4.4  ```project_theme_lightbox``` integration

- The magnific popup integration is deprecated and will be removed in later versions.
- Implement JavaScript according to the readme after integrating ```project_theme_lightbox```

## Version 4 Breaking Changes

- Dropped TYPO3 8 support
