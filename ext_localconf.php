<?php

use SGalinski\SgYoutube\Controller\YoutubeController;
use SGalinski\SgYoutube\Form\Element\LicenceStatus;
use SGalinski\SgYoutube\Hooks\LicenceCheckHook;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

if ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_youtube']['uncached'] ?? FALSE) {
	// Uncached version
	ExtensionUtility::configurePlugin(
		'SgYoutube',
		'Youtube',
		[
			YoutubeController::class => 'index',
		],
		[
			YoutubeController::class => 'index',
		],
	);
} else {
	// Cached version
	ExtensionUtility::configurePlugin(
		'SgYoutube',
		'Youtube',
		[
			YoutubeController::class => 'index',
		]
	);
}

$currentTypo3Version = VersionNumberUtility::getCurrentTypo3Version();
if (version_compare($currentTypo3Version, '13.0.0', '<')) {
	// include Plugin sg_youtube
	ExtensionManagementUtility::addPageTSConfig(
		'@import "EXT:sg_youtube/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig"'
	);
}

// Caching
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['sgyoutube_cache'] ??= [];

// Add licenceCheck RenderType
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][] = [
	'nodeName' => 'SgYoutubeLicenceCheck',
	'priority' => 40,
	'class' => LicenceStatus::class,
];
